/**
 * @author sasikala
 * @created on 11/25/2020
 * @description class for testing log wrapper implementation
 * @version number v1.0
 */

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;

import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		RRAdapter logger = new RRAdapterImpl(Main.class.getName());
		Map<String, String> additionalParam = new HashMap<String, String>();
		additionalParam.put("requestingService", "ui-service");
		logger.info("123", "cvcm", "Message published to kafka: pvci_ocr_output1", additionalParam);
	}
}
