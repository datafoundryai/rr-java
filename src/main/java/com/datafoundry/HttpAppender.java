package com.datafoundry;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import net.logstash.logback.encoder.LogstashEncoder;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

/**
 * @author sasikala
 * @version number v1.0
 * @created on 12/22/2020
 * @description custom http appender for publishing log events to http service
 */
public class HttpAppender extends AppenderBase<ILoggingEvent> {
    private LogstashEncoder encoder;
    private Config config = ConfigFactory.load();
    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    protected void append(ILoggingEvent eventObject) {
        if(encoder!=null){
            String logEvent = new String(encoder.encode(eventObject));
            publishMessage(logEvent);
        }
    }

    private void publishMessage(String message) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(message))
                .uri(URI.create(config.getString("dpaService.url")))
                .header("Content-Type", "application/json")
                .build();
        try {
            httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception ex){
            // TODO need to discuss on what to handle here
        }
    }

    public LogstashEncoder getEncoder() {
        return encoder;
    }

    public void setEncoder(LogstashEncoder encoder) {
        this.encoder = encoder;
    }
}
