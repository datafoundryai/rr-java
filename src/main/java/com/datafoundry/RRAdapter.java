package com.datafoundry;

import java.util.Map;

/**
 * @author sasikala
 * @version number v1.0
 * @created on 11/21/2020
 * @description Interface for wrapper class for structured logging
 */
public interface RRAdapter {
    void info(String rrId, String source, String message, Map<String, String> optionalParam);
    void warn(String rrId, String source, String message, Map<String, String> optionalParam);
    void debug(String rrId, String source, String message, Map<String, String> optionalParam);
    void error(String rrId, String source, String message, Map<String, String> optionalParam);
}
