package com.datafoundry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Map;

/**
 * @author sasikala
 * @version number v1.0
 * @created on 11/21/2020
 * @description wrapper class for structured logging
 */
public class RRAdapterImpl implements RRAdapter {
    private Logger logger;

    public RRAdapterImpl(String className){
        logger = LoggerFactory.getLogger(className);
    }

    public void info(String rrId, String source, String message, Map<String, String> optionalParam){
        addCustomLogParameters(rrId, source, optionalParam);
        logger.info(message);
        MDC.clear();
    }

    public void warn(String rrId, String source, String message, Map<String, String> optionalParam) {
        addCustomLogParameters(rrId, source, optionalParam);
        logger.warn(message);
        MDC.clear();
    }

    public void debug(String rrId, String source, String message, Map<String, String> optionalParam) {
        addCustomLogParameters(rrId, source, optionalParam);
        logger.debug(message);
        MDC.clear();
    }

    public void error(String rrId, String source, String message, Map<String, String> optionalParam) {
        addCustomLogParameters(rrId, source, optionalParam);
        logger.error(message);
        MDC.clear();
    }

    private void addCustomLogParameters(String rrId, String source, Map<String, String> optionalParam){
        MDC.put("rrId", rrId);
        MDC.put("source", source);

        if(optionalParam != null && !optionalParam.isEmpty()){
            for (String key : optionalParam.keySet()) {
                MDC.put(key,optionalParam.get(key));
            }
        }
    }

}
