**rr-java logger library**

##### Adding rr-java logger as a dependency to your application
1. Add the below lines to your application pom.xml to get rr-java dependency:

        <dependency>
            <groupId>com.datafoundry</groupId>
            <artifactId>rr-java</artifactId>
            <version>1.0-SNAPSHOT</version>
            <exclusions>
                <exclusion>
                     <groupId>ch.qos.logback</groupId>
                    <artifactId>logback-classic</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-api</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
2.  Add a property file named app_info.properties at src/main/resources folder and add the below lines:
       
           APP_NAME=<your application name> 
           LOG_LEVEL=INFO
3. In case of spring boot application also add exclusion tag to your spring-boot-starter or starter-web dependency as below, this is to avoid jar conflicts:
    
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

##### Testing in your local

1. In order to test in your local, place settings.xml at your local maven repository path ${user.home}/.m2/settings.xml
   
   Copy the file from: http://repo.dfoundry.io/projects/RR/repos/rr-java/browse/maven-setting?at=refs%2Fheads%2Fdev
2. Set an environment variable in your IDE and terminal:
    
        REACT_APP_APIURL: https://api.df-dev.net
        
        terminal level command for windows: 
        set REACT_APP_APIURL=https://api.df-dev.net
3. Now run the below command from the terminal:

        mvn clean install


##### How to use rr-java logger to log messages in your application
1. Instantiate the logger instance as below:

        RRAdapter logger = new RRAdapterImpl(this.getClass().getName());

2. Using different rr logger methods:

   2.1 Available methods are: info, warn, debug and error
   
   2.2 Method signature:
          The method consists of 4 parameters,
          
            1.	rrId: String – This is a unique id to track a request/ process end to end. 
               Map rrId from the request header, if not present generate a random uuid version 4.
            2.	source: String –  Use this parameter to map either solution name (ex: cvcm, pvcm) 
                                 or service name (ex: dpa-service, scheduler-service)
            3.	message: String – Use this parameter to log any informational or exception message.
                                 example: info - “Message published to kafka: topic1”
                                          error - “Uncaught exception occurred: json parse exception”
            4.	optionalParam: Map<String, String> – Use this parameter to log any additional key-value data or pass null
3. Sample log message:

        Map<String, String> optionalParam = new HashMap<String, String>();
        optionalParam.put("requestingService", "ui-service");
        
        logger.info("f59398ed-c03e-4ad6-8199-e3c2f988fb06","cvcm","Message published to Kafka",optionalParam)
        
        {"timestamp":"2020-12-22T16:44:33.235+05:30","message":"Message published to Kafka","logger_name":"Main","thread_name":"main","level":"INFO","requestingService":"ui-service","source":"cvcm","rrId":"f59398ed-c03e-4ad6-8199-e3c2f988fb06","app":"testApp"}
   
On successful integration you should see structured logging on the **console** and in the **log file** that would be generated within a log folder at the root of your project (log/your-app-name.log)


